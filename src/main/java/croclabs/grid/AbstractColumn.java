package croclabs.grid;

import java.util.ArrayList;
import java.util.Collection;

public abstract class AbstractColumn<ID, V, T extends Enum<?>> implements IColumn<ID, V, T> {
	protected ID id;
	protected String name;
	protected T type;
	protected boolean sortable;
	protected boolean visible;
	protected Collection<V> values = new ArrayList<>();

	@Override
	public ID getId() {
		return id;
	}

	@Override
	public IColumn<ID, V, T> setId(ID id) {
		this.id = id;
		return this;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public IColumn<ID, V, T> setName(String name) {
		this.name = name;
		return this;
	}

	@Override
	public T getType() {
		return type;
	}

	@Override
	public IColumn<ID, V, T> setType(T type) {
		this.type = type;
		return this;
	}

	@Override
	public boolean isVisible() {
		return visible;
	}

	@Override
	public IColumn<ID, V, T> setVisible(boolean visible) {
		this.visible = visible;
		return this;
	}

	@Override
	public boolean isSortable() {
		return sortable;
	}

	@Override
	public IColumn<ID, V, T> setSortable(boolean sortable) {
		this.sortable = sortable;
		return this;
	}

	@Override
	public Collection<V> getValues() {
		return values;
	}
}
