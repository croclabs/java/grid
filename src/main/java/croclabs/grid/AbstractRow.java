package croclabs.grid;

import java.util.ArrayList;
import java.util.Collection;

public abstract class AbstractRow implements IRow {
	Collection<Object> values = new ArrayList<>();

	@Override
	public Collection<Object> getValues() {
		return values;
	}
}
