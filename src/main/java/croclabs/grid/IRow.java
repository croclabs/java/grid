package croclabs.grid;

import java.util.Arrays;
import java.util.Collection;

public interface IRow {
	default IRow addValues(Object... value) {
		getValues().addAll(Arrays.asList(value));
		return this;
	}
	Collection<Object> getValues();
}
