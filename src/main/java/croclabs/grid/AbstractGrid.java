package croclabs.grid;

import java.util.ArrayList;
import java.util.Collection;

public abstract class AbstractGrid<ID> implements IGrid<ID> {
	protected ID id;
	protected Collection<IColumn<?, ?, ? extends Enum<?>>> columns = new ArrayList<>();

	@Override
	public ID getId() {
		return id;
	}

	@Override
	public Collection<IColumn<?, ?, ? extends Enum<?>>> getColumns() {
		return columns;
	}

	@Override
	public AbstractGrid<ID> setId(ID id) {
		this.id = id;
		return this;
	}

	@Override
	public AbstractGrid<ID> fromJson(Object json) {
		return (AbstractGrid<ID>) IGrid.super.fromJson(json);
	}
}
