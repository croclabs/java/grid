package croclabs.grid;

import java.util.Arrays;
import java.util.Collection;

@SuppressWarnings("all")
public interface IColumn<ID, V, T extends Enum<?>> {
	ID getId();
	IColumn<ID, V, T> setId(ID id);

	String getName();
	IColumn<ID, V, T> setName(String name);

	T getType();
	IColumn<ID, V, T> setType(T type);

	boolean isVisible();
	IColumn<ID, V, T> setVisible(boolean visible);

	boolean isSortable();
	IColumn<ID, V, T> setSortable(boolean sortable);

	Collection<V> getValues();
	default IColumn<ID, V, T> addValues(V... value) {
		getValues().addAll(Arrays.asList(value));
		return this;
	}

	default IColumn<ID, V, T> addValues(Value<V> value) {
		value.value(getValues());
		return (IColumn<ID, V, T>) this;
	}

	@FunctionalInterface
	interface Value<V> {
		void value(Collection<V> values);
	}
}
