package croclabs.grid;

import croclabs.json.JSON;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@SuppressWarnings("all")
public interface IGrid<ID> {
	ID getId();

	IGrid<ID> setId(ID id);

	Collection<IColumn<?, ?, ? extends Enum<?>>> getColumns();

	default IGrid<ID> addColumns(Collection<IColumn<?, ?, ? extends Enum<?>>> columns) {
		getColumns().addAll(columns);
		return this;
	}

	default IGrid<ID> addColumns(ColumnAdd column) {
		column.column(getColumns());
		return this;
	}

	default IGrid<ID> fromJson(Object json) {
		return JSON.parser(json).target(this).parse();
	}

	default String json() {
		return JSON.composer(this).pretty().compose();
	}

	default Collection<IRow> getRows() {
		List<IRow> rows = new ArrayList<>();

		getColumns().forEach(column -> {
			AtomicInteger counter = new AtomicInteger();

			column.getValues().forEach(value -> {
				if (rows.size() <= counter.get()) {
					rows.add(new Row());
				}

				IRow row = rows.get(counter.get());
				row.addValues(value);
				counter.getAndIncrement();
			});
		});

		return rows;
	}

	default IGrid<ID> clear() {
		getColumns().clear();
		return this;
	}

	@FunctionalInterface
	interface ColumnAdd {
		void column(Collection<IColumn<?, ?, ? extends Enum<?>>> columns);
	}
}
