package croclabs.grid;

import org.junit.jupiter.api.Test;

import java.rmi.server.UID;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class GridTest {
	enum TestTypes {
		LONG, STRING
	}

	@Test
	void grid(){
		Grid<UUID> grid = (Grid<UUID>) new Grid<UUID>().setId(UUID.randomUUID()).addColumns(
				List.of(new Column<UUID, Long, TestTypes>()
								.setId(UUID.randomUUID())
								.setName("1")
								.setSortable(true)
								.setVisible(true)
								.setType(TestTypes.LONG)
								.addValues(1L,1L),
						new Column<>()
								.setId(UUID.randomUUID())
								.setName("1")
								.setType(TestTypes.STRING)
								.addValues("2L","2L"),
						new Column<>()
								.setId(UUID.randomUUID())
								.setName("1")
								.setType(TestTypes.LONG)
								.addValues(3L,3L)));

		String json = grid.json();
		assertNotNull(json);

		grid.clear();

		grid.setId(UUID.randomUUID()).addColumns(
				columns -> columns.addAll(List.of(new Column<UUID, Long, TestTypes>()
							.setId(UUID.randomUUID())
							.setName("1")
							.setType(TestTypes.LONG)
							.addValues(1L,1L),
					new Column<>()
							.setId(UUID.randomUUID())
							.setName("1")
							.setType(TestTypes.STRING)
							.addValues(values -> values.addAll(List.of("2L","2L"))),
					new Column<>()
							.setId(UUID.randomUUID())
							.setName("1")
							.setType(TestTypes.LONG)
							.addValues(3L,3L))));

		json = grid.json();
		assertNotNull(json);
	}
}